#include <mgos.h>
#include "mgos_http_server.h"
#include <sys/stat.h>

bool f_exists(const char *name)
{
  struct stat buffer;
  return (stat(name, &buffer) == 0);
}

static struct mg_serve_http_opts s_http_server_opts;
struct mg_serve_http_opts opts_cpy;

static struct http_message *handle_gzip(struct mg_connection *nc, struct http_message *msg,
                                        struct mg_serve_http_opts *opts)
{
  struct mg_str uri = mg_mk_str_n(msg->uri.p, msg->uri.len);
  char *uri_s = strdup(uri.p);
  if (f_exists(uri_s))
  {
    free(uri_s);
    return NULL;
  }
  free(uri_s);

  char uri_gziped[msg->uri.len + 10];
  memset(uri_gziped, '\0', msg->uri.len + 10);
  strncat(uri_gziped, uri.p, uri.len);
  strcat(uri_gziped, ".gz");

  // LOG(LL_INFO, ("REQ         uri o: \"%.*s\"\n", msg->uri.len, msg->uri.p));
  // LOG(LL_INFO, ("REQ         uri gz: \"%s\"\n", uri_gziped));
  if (f_exists(uri_gziped))
  {
    struct http_message *msg2 = (struct http_message *)malloc(sizeof(struct http_message));
    memcpy(msg2, msg, sizeof(struct http_message));
    char *mallocked = strdup(uri_gziped);
    msg2->uri.p = mallocked;
    msg2->uri.len = strlen(mallocked);
    return msg2;
  }

  return NULL;
}

static void root_handler(struct mg_connection *nc, int ev, void *p, void *user_data)
{
  (void)user_data;
  if (ev != MG_EV_HTTP_REQUEST)
  {
    return;
  }
  // LOG(LL_INFO, ("root_handler"));
  struct http_message *msg = (struct http_message *)(p);
  // http_msg_print(msg);

  struct http_message *ret = handle_gzip(nc, msg, &opts_cpy);
  if (ret)
  {
    LOG(LL_DEBUG, ("REQ         uri docked\n"));
    // LOG(LL_INFO, ("REQ         uri: \"%.*s\"\n", ret->uri.len, ret->uri.p));
    mg_serve_http(nc, ret, opts_cpy);
    free((void *)ret->uri.p);
    free((void *)ret);
  }
  else
  {
    LOG(LL_DEBUG, ("REQ         uri NOT docked\n"));
    mg_serve_http(nc, msg, s_http_server_opts);
  }
}

bool mgos_http_gzip_init(void)
{
  memset(&s_http_server_opts, 0, sizeof(s_http_server_opts));
  s_http_server_opts.document_root = mgos_sys_config_get_http_document_root();
  /*
   * add mime types for css.gz and js.gz
   */
  s_http_server_opts.custom_mime_types = strdup(mgos_sys_config_get_http_gzip_intercepted_mimes());
      // ".js.gz=application/javascript; charset=utf-8,.css.gz=text/css; charset=utf-8";
  /*
   * more options can be filled in
   */

  mgos_register_http_endpoint("/", root_handler, NULL);

  memcpy(&opts_cpy, &s_http_server_opts, sizeof(opts_cpy));
  opts_cpy.extra_headers = "Content-Encoding: gzip";

  return true;
}
